package tt.showcase.spring.common;

import java.util.Collection;

import org.springframework.dao.DataAccessException;

public interface BaseRepository<T1, T2> {

	Collection<T1> findAll() throws DataAccessException;

	void delete(T1 entity) throws DataAccessException;

	T1 findById(T2 id) throws DataAccessException;

	T1 save(T1 entity) throws DataAccessException;
}
