package tt.showcase.spring.common;

import java.io.Serializable;

import org.springframework.data.repository.CrudRepository;
/**
 * 
 * @author jakubp
 * 
 */
public interface BaseSpringJpaRepository<T1 extends AbstractEntity, T2 extends Serializable>
		extends CrudRepository<T1, T2> {
}
