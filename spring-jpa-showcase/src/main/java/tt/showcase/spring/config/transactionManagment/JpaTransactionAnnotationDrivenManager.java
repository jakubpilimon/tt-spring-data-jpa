package tt.showcase.spring.config.transactionManagment;

import javax.persistence.EntityManagerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import tt.showcase.spring.config.TransactionManagerResolver;

@Configuration
@EnableTransactionManagement
public class JpaTransactionAnnotationDrivenManager implements
		TransactionManagerResolver {

	@Autowired
	private EntityManagerFactory emf;

	@Override
	@Bean
	public PlatformTransactionManager transactionManager() {
		return new org.springframework.orm.jpa.JpaTransactionManager(emf);
	}
}
