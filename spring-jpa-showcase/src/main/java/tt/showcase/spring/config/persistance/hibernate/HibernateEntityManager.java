package tt.showcase.spring.config.persistance.hibernate;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;

import tt.showcase.spring.config.EntityManagerFactoryResolver;

@Configuration
@PropertySource("classpath:/persistance/jpa.properties")
public class HibernateEntityManager implements EntityManagerFactoryResolver {

	@Value("#{ environment['jpa.showSql'] }")
	private Boolean showSql;
	@Value("#{ environment['jpa.database'] }")
	private Database database;
	@Value("#{ environment['jpa.persistanceUnitname'] }")
	private String persistanceUnitName;

	@Autowired
	private Environment environment;

	@Autowired
	private DataSource dataSource;

	@Override
	@Bean
	public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
		HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
		vendorAdapter.setDatabase(database);
		vendorAdapter.setShowSql(showSql);
		vendorAdapter.setGenerateDdl(true);

		LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();
		factory.setPersistenceUnitName(persistanceUnitName);
		factory.setJpaVendorAdapter(vendorAdapter);
		factory.setPackagesToScan("tt.showcase.spring");
		factory.setDataSource(dataSource);
		return factory;
	}

}
