package tt.showcase.spring.config;

import javax.sql.DataSource;

public interface DataSourceResolver {
	DataSource dataSource();
}
