package tt.showcase.spring.config;

import org.springframework.transaction.PlatformTransactionManager;

public interface TransactionManagerResolver {
	PlatformTransactionManager transactionManager();
}
