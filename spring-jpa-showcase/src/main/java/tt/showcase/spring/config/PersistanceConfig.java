package tt.showcase.spring.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableJpaRepositories(basePackages = "tt.showcase.spring")
public class PersistanceConfig {

	@Autowired
	private DataSourceResolver dataSource;

	@Autowired
	private EntityManagerFactoryResolver entityManagerFactory;

	@Autowired
	private TransactionManagerResolver transactionManager;

	@Bean
	public PersistenceExceptionTranslationPostProcessor persistenceExceptionTranslationPostProcessor() {
		PersistenceExceptionTranslationPostProcessor exceptionTranslator = new PersistenceExceptionTranslationPostProcessor();
		return exceptionTranslator;
	}

}