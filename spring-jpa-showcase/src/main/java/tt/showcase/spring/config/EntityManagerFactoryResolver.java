package tt.showcase.spring.config;

import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;

public interface EntityManagerFactoryResolver {
	LocalContainerEntityManagerFactoryBean entityManagerFactory();
}
