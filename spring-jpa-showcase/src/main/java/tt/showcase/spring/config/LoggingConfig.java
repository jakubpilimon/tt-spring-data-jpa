package tt.showcase.spring.config;

import org.springframework.aop.Advisor;
import org.springframework.aop.aspectj.AspectJExpressionPointcut;
import org.springframework.aop.interceptor.CustomizableTraceInterceptor;
import org.springframework.aop.support.DefaultPointcutAdvisor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@Configuration
@EnableAspectJAutoProxy
public class LoggingConfig {

	@Bean
	public CustomizableTraceInterceptor springJpaRepositoriesInterceptor() {

		CustomizableTraceInterceptor springJpaMethodsInterceptor = new CustomizableTraceInterceptor();
		springJpaMethodsInterceptor.setLoggerName("springJpaRepos");
		springJpaMethodsInterceptor
				.setEnterMessage("Entering $[targetClassShortName].$[methodName] with ($[arguments]).");
		springJpaMethodsInterceptor  
				.setExceptionMessage("Exception occured: $[exception]");
		springJpaMethodsInterceptor
				.setExitMessage("Leaving $[targetClassShortName].$[methodName] and return value is $[returnValue], invocation time: $[invocationTime]ms.");

		return springJpaMethodsInterceptor;
	}

	@Bean
	public Advisor springJpaRepositoriesTraceAdvisor() {

		AspectJExpressionPointcut springJpaRepositoryPointCut = new AspectJExpressionPointcut();
		springJpaRepositoryPointCut
				.setExpression("execution(public * tt.showcase.spring.common.BaseSpringJpaRepository+.*(..))");
		return new DefaultPointcutAdvisor(springJpaRepositoryPointCut,
				springJpaRepositoriesInterceptor());
	}
}
