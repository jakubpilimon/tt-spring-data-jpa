package tt.showcase.spring.config.persistance.hsql;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

import tt.showcase.spring.config.DataSourceResolver;

@Configuration
@PropertySource("classpath:/persistance/hsql/hsql.properties")
public class EmbeddedHsqlDataSource implements DataSourceResolver {

	private static final String SCRIPT_DATA_LOCATION = "script.dataLocation";
	private static final String SCRIPT_INIT_LOCATION = "script.initLocation";

	@Autowired
	private Environment environment;

	@Bean
	public DataSource dataSource() {
		return new EmbeddedDatabaseBuilder().setType(EmbeddedDatabaseType.HSQL)
				.addScript(environment.getProperty(SCRIPT_INIT_LOCATION))
				.addScript(environment.getProperty(SCRIPT_DATA_LOCATION))
				.build();
	}

}
