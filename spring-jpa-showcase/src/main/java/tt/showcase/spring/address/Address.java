package tt.showcase.spring.address;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import tt.showcase.spring.common.AbstractEntity;

@Entity
@Table(name = "ADDRESSES")
public class Address extends AbstractEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ADDRESS_ID", updatable = false, insertable = false)
	private Integer id;

	@Column(name = "STREET", unique = true)
	private String street;

	@Column(name = "CITY", unique = true)
	private String city;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	@Override
	public String toString() {
		return "{" + street + " " + city + "}";
	}
}
