package tt.showcase.spring.address.springjpa;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import tt.showcase.spring.address.Address;
import tt.showcase.spring.address.AddressRepository;
import tt.showcase.spring.common.BaseSpringJpaRepository;

/**
 * W UserRepository wszystkie metody wyciągają rekord lub kolekcję rekordów.
 * Tutaj są przykłady metod modyfikujących zawartość bazy danych.
 * 
 * @author jakubp
 * 
 */
@Transactional(readOnly = true)
public interface AddressSpringJpaRepository extends AddressRepository,
		BaseSpringJpaRepository<Address, Integer> {

	@Modifying
	@Transactional
	@Query("update Address a set a.city = ?1 where a.street = ?2")
	int setFixedCityForStreet(String city, String street);
}
