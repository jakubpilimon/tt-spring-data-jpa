package tt.showcase.spring.address;

import tt.showcase.spring.common.BaseRepository;

public interface AddressRepository extends BaseRepository<Address, Integer> {

	int setFixedCityForStreet(String city, String street);
}
