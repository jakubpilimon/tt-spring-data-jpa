package tt.showcase.spring.address.springjpa;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import tt.showcase.spring.address.Address;

public interface AddressPagingAndSortingRepository extends
		PagingAndSortingRepository<Address, Integer> {

	List<Address> findByCity(String city, Pageable page);

}
