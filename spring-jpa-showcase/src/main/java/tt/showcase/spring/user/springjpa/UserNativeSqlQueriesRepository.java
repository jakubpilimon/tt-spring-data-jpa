package tt.showcase.spring.user.springjpa;

import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import tt.showcase.spring.user.User;
import tt.showcase.spring.user.UserRepository;

@Qualifier("nativeQueries")
public interface UserNativeSqlQueriesRepository extends UserRepository,
		JpaRepository<User, Integer> {

	public static final String SELECT_ALL_FROM_USERS = "SELECT * FROM USERS ";

	@Query(value = SELECT_ALL_FROM_USERS + "U WHERE U.IP_ADDRESS = ?1", nativeQuery = true)
	User findByIpAddress(String ipAddress) throws DataAccessException;

	@Query(value = SELECT_ALL_FROM_USERS
			+ "U WHERE U.FIRST_NAME = ?1 AND U.LAST_NAME = ?2", nativeQuery = true)
	User findByFirstnameAndLastname(String firstname, String lastname)
			throws DataAccessException;

	@Query(value = SELECT_ALL_FROM_USERS
			+ "U WHERE U.FIRST_NAME = ?1 OR U.LAST_NAME = ?2", nativeQuery = true)
	Collection<User> findByFirstnameOrLastname(String firstname, String lastname)
			throws DataAccessException;

	@Query(value = SELECT_ALL_FROM_USERS + "U WHERE U.AGE > ?1 AND U.AGE < ?2", nativeQuery = true)
	Collection<User> findByAgeBetween(Integer minAge, Integer maxAge)
			throws DataAccessException;

	@Query(value = SELECT_ALL_FROM_USERS + "U WHERE U.CREATION_DATE > ?1", nativeQuery = true)
	Collection<User> findByCreationDateAfter(Date startDate)
			throws DataAccessException;

	@Query(value = SELECT_ALL_FROM_USERS
			+ "U WHERE U.MODIFICATION_DATE IS NULL", nativeQuery = true)
	Collection<User> findByModificationDateIsNull() throws DataAccessException;

	@Query(value = SELECT_ALL_FROM_USERS + "U WHERE U.FIRST_NAME LIKE ?1", nativeQuery = true)
	Collection<User> findByFirstnameStartingWith(String firstname)
			throws DataAccessException;

	@Query(value = SELECT_ALL_FROM_USERS + "U WHERE U.AGE IN ?1", nativeQuery = true)
	Collection<User> findByAgeIn(Collection<Integer> ages)
			throws DataAccessException;

	@Query(value = SELECT_ALL_FROM_USERS + "U WHERE U.ACTIVE = TRUE", nativeQuery = true)
	Collection<User> findByActiveTrue() throws DataAccessException;
}
