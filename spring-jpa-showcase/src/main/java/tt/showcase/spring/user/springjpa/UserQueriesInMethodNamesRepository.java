package tt.showcase.spring.user.springjpa;

import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.transaction.annotation.Transactional;

import tt.showcase.spring.common.BaseSpringJpaRepository;
import tt.showcase.spring.user.User;
import tt.showcase.spring.user.UserRepository;

@Transactional(readOnly = true)
@Qualifier("methodNamesQueries")
public interface UserQueriesInMethodNamesRepository extends UserRepository,
		BaseSpringJpaRepository<User, Integer> {

	/* IpAddressNot... */
	User findByIpAddress(String ipAddress) throws DataAccessException;

	User findByFirstnameAndLastname(String firstname, String lastname)
			throws DataAccessException;

	Collection<User> findByFirstnameOrLastname(String firstname, String lastname)
			throws DataAccessException;

	/* GreaterThan, LessThan... */
	Collection<User> findByAgeBetween(Integer minAge, Integer maxAge)
			throws DataAccessException;

	/* Before, Between... */

	Collection<User> findByCreationDateAfter(Date startDate)
			throws DataAccessException;

	/* NotNull... */
	Collection<User> findByModificationDateIsNull() throws DataAccessException;

	/* Ending with, Containing, Like, NotLike... */
	Collection<User> findByFirstnameStartingWith(String firstname)
			throws DataAccessException;

	Collection<User> findByAgeIn(Collection<Integer> ages)
			throws DataAccessException;

	Collection<User> findByActiveTrue() throws DataAccessException;
	/* OrderByXXXDesc findByXXXOrderByZZZ, etc... */

}
