package tt.showcase.spring.user.custom;

import tt.showcase.spring.user.User;
import tt.showcase.spring.user.UserCustomRepository;

public class UserCustomRepositoryImpl implements UserCustomRepository {

	public void doAnythingWithUser(User user) {
		user.setFirstname("unknown");
	}
}
