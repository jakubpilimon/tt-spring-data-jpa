package tt.showcase.spring.user;

public interface UserCustomRepository {

	void doAnythingWithUser(User user);
}
