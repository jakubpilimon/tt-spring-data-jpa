package tt.showcase.spring.user.springjpa.specifications;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import tt.showcase.spring.common.BaseSpringJpaRepository;
import tt.showcase.spring.user.User;

public interface UserJpaSpecificationsRepository extends
		BaseSpringJpaRepository<User, Integer>, JpaSpecificationExecutor<User> {

}
