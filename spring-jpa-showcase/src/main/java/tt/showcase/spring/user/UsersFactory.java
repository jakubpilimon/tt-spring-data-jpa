package tt.showcase.spring.user;

import java.util.Random;

public final class UsersFactory {

	private UsersFactory() {
	}

	public static User createActiveUserWithRandomAge(String firstname,
			String lastname) {
		User user = new User();
		user.setFirstname(firstname);
		user.setLastname(lastname);
		user.setActive(true);
		user.setIpAddress(generateRandomIpAddress());
		user.setAge(generateRandomAgeLessThan(100));
		return user;
	}

	private static Integer generateRandomAgeLessThan(int i) {
		Random r = new Random();
		return r.nextInt(100);
	}

	private static String generateRandomIpAddress() {
		Random r = new Random();
		return r.nextInt(256) + "." + r.nextInt(256) + "." + r.nextInt(256)
				+ "." + r.nextInt(256);
	}

}
