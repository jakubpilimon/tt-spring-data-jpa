package tt.showcase.spring.user.springjpa;

import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import tt.showcase.spring.user.User;
import tt.showcase.spring.user.UserRepository;

@Qualifier("jpaQueries")
public interface UserJpaQueriesRepository extends UserRepository,
		JpaRepository<User, Integer> {

	public static final String SELECT_EVERYTHING_FROM_ENTITY = "select u from #{#entityName} ";

	@Query(SELECT_EVERYTHING_FROM_ENTITY + "u where u.ipAddress = ?1")
	User findByIpAddress(String ipAddress) throws DataAccessException;

	@Query(SELECT_EVERYTHING_FROM_ENTITY
			+ "u where u.firstname = :firstname and u.lastname = :lastname")
	User findByFirstnameAndLastname(@Param("firstname") String firstname,
			@Param("lastname") String lastname) throws DataAccessException;

	@Query(SELECT_EVERYTHING_FROM_ENTITY
			+ "u where u.firstname = :firstname or u.lastname = :lastname")
	Collection<User> findByFirstnameOrLastname(
			@Param("firstname") String firstname,
			@Param("lastname") String lastname) throws DataAccessException;

	@Query(SELECT_EVERYTHING_FROM_ENTITY
			+ "u where u.age > :minAge and u.age < :maxAge")
	Collection<User> findByAgeBetween(@Param("minAge") Integer minAge,
			@Param("maxAge") Integer maxAge) throws DataAccessException;

	@Query(SELECT_EVERYTHING_FROM_ENTITY + "u where u.creationDate > ?1")
	Collection<User> findByCreationDateAfter(Date startDate)
			throws DataAccessException;

	@Query(SELECT_EVERYTHING_FROM_ENTITY + "u where u.modificationDate is null")
	Collection<User> findByModificationDateIsNull() throws DataAccessException;

	@Query(SELECT_EVERYTHING_FROM_ENTITY + "u where u.firstname like ?1")
	Collection<User> findByFirstnameStartingWith(String firstname)
			throws DataAccessException;

	@Query(SELECT_EVERYTHING_FROM_ENTITY + "u where u.age in ?1")
	Collection<User> findByAgeIn(Collection<Integer> ages)
			throws DataAccessException;

	@Query(SELECT_EVERYTHING_FROM_ENTITY + "u where u.active = true")
	Collection<User> findByActiveTrue() throws DataAccessException;
}
