package tt.showcase.spring.user.springjpa.specifications;

import java.util.Date;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import tt.showcase.spring.user.User;
import tt.showcase.spring.user.User_;

public class UserSpecifications {

	public static Specification<User> isCreatedBefore(final Date date) {
		return new Specification<User>() {
			public Predicate toPredicate(Root<User> root,
					CriteriaQuery<?> query, CriteriaBuilder builder) {

				return builder.lessThan(root.get(User_.creationDate), date);
			}
		};
	}

	public static Specification<User> hasFirstName(final String firstName) {
		return new Specification<User>() {
			public Predicate toPredicate(Root<User> root,
					CriteriaQuery<?> query, CriteriaBuilder builder) {
				return builder.equal(root.get(User_.firstname), firstName);
			}
		};
	}

	public static Specification<User> atAge(final Integer age) {
		return new Specification<User>() {
			public Predicate toPredicate(Root<User> root,
					CriteriaQuery<?> query, CriteriaBuilder builder) {

				return builder.equal(root.get(User_.age), age);
			}
		};
	}

	public static Specification<User> atAgeBetween(final Integer minAge,
			final Integer maxAge) {
		return new Specification<User>() {
			public Predicate toPredicate(Root<User> root,
					CriteriaQuery<?> query, CriteriaBuilder builder) {

				return builder.between(root.get(User_.age), minAge, maxAge);
			}
		};
	}
}
