package tt.showcase.spring.user;

import java.util.Collection;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import tt.showcase.spring.address.Address;
import tt.showcase.spring.common.AbstractEntity;

@Entity
@Table(name = "USERS")
@EntityListeners(AuditingEntityListener.class)
public class User extends AbstractEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "USER_ID", updatable = false, insertable = false)
	private Integer id;

	@Column(name = "IP_ADDRESS", unique = true)
	private String ipAddress;

	@Column(name = "FIRST_NAME")
	private String firstname;

	@Column(name = "LAST_NAME")
	private String lastname;

	@Column(name = "AGE")
	private Integer age;

	@CreatedDate
	@Column(name = "CREATION_DATE")
	private Date creationDate;

	@LastModifiedDate
	@Column(name = "MODIFICATION_DATE")
	private Date modificationDate;

	@JoinColumn(name = "USER_ID")
	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
	private Collection<Address> addresses;

	@Column(name = "ACTIVE")
	private Boolean active;

	@Override
	public String toString() {
		return "[id=" + id + " " + "firstname=" + firstname + " " + "lastname="
				+ lastname + " " + "ipAddress=" + ipAddress + "]";
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getModificationDate() {
		return modificationDate;
	}

	public void setModificationDate(Date modificationDate) {
		this.modificationDate = modificationDate;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}
}
