package tt.showcase.spring.user;

import java.util.Collection;
import java.util.Date;

import org.springframework.dao.DataAccessException;

import tt.showcase.spring.common.BaseRepository;

public interface UserRepository extends BaseRepository<User, Integer> {

	User findByIpAddress(String ipAddress) throws DataAccessException;

	User findByFirstnameAndLastname(String firstname, String lastname)
			throws DataAccessException;

	Collection<User> findByFirstnameOrLastname(String string, String string2)
			throws DataAccessException;

	Collection<User> findByAgeBetween(Integer minAge, Integer maxAge)
			throws DataAccessException;

	Collection<User> findByCreationDateAfter(Date startDate)
			throws DataAccessException;

	Collection<User> findByModificationDateIsNull() throws DataAccessException;

	Collection<User> findByFirstnameStartingWith(String firstname)
			throws DataAccessException;

	Collection<User> findByAgeIn(Collection<Integer> ages)
			throws DataAccessException;

	Collection<User> findByActiveTrue() throws DataAccessException;
}
