package tt.showcase.spring.test.address.repositories;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import tt.showcase.spring.address.AddressRepository;
import tt.showcase.spring.test.common.AbstractSpringTest;

public class AddressRepositoryTest extends AbstractSpringTest {

	@Autowired
	private AddressRepository addressRepository;

	@Test
	public void modifyCityOfStreetAndCheckResult() {
		int addressesWithPawiaStreetCount = addressRepository
				.setFixedCityForStreet("Warszawa", "Pawia");
		assertThat(addressesWithPawiaStreetCount, is(1));
	}
}
