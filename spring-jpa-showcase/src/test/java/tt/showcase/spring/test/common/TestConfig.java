package tt.showcase.spring.test.common;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import tt.showcase.spring.config.AppConfig;

@Configuration
@Import(AppConfig.class)
public class TestConfig {
}
