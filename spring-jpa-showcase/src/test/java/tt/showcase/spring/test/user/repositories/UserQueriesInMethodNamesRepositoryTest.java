package tt.showcase.spring.test.user.repositories;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import tt.showcase.spring.user.UserRepository;

public class UserQueriesInMethodNamesRepositoryTest extends BaseUserRepositoryTest {

	@Autowired
	@Qualifier("methodNamesQueries")
	private UserRepository userRepository;

	@Override
	public UserRepository getUserRepository() {
		return userRepository; 
	}

}
