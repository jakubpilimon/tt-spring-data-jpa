package tt.showcase.spring.test.user.auditing;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.lessThan;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;

import java.util.Date;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import tt.showcase.spring.test.common.AbstractSpringTest;
import tt.showcase.spring.user.User;
import tt.showcase.spring.user.UserRepository;
import tt.showcase.spring.user.UsersFactory;

public class UserAuditingTest extends AbstractSpringTest {

	@Autowired
	@Qualifier("methodNamesQueries")
	private UserRepository userRepository;

	@Test
	public void checkWhenNewUserInserted_creationDateIsFilled() {
		User user = UsersFactory.createActiveUserWithRandomAge("Luke",
				"Skywalker");
		assertNull(user.getCreationDate());
		user = userRepository.save(user);
		assertNotNull(user.getCreationDate());
	}

	@Test
	public void checkWhenUserIsModified_modificationDateHasChanged() {
		User user = findUserLocalhost();
		Date previousModification = user.getModificationDate();
		modifyUserFirstname(user);
		user = userRepository.save(user);
		assertThat(previousModification.compareTo(user.getModificationDate()),
				is(lessThan(0)));
	}

	private User findUserLocalhost() {
		return userRepository.findByIpAddress("127.0.0.1");
	}

	private void modifyUserFirstname(User user) {
		user.setFirstname("localhost");
	}

}
