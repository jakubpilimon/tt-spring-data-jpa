package tt.showcase.spring.test.user.repositories.specifications;

import static org.hamcrest.Matchers.both;
import static org.hamcrest.Matchers.either;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.lessThan;
import static org.junit.Assert.assertThat;
import static org.springframework.data.jpa.domain.Specifications.where;
import static tt.showcase.spring.user.springjpa.specifications.UserSpecifications.atAge;
import static tt.showcase.spring.user.springjpa.specifications.UserSpecifications.atAgeBetween;
import static tt.showcase.spring.user.springjpa.specifications.UserSpecifications.hasFirstName;
import static tt.showcase.spring.user.springjpa.specifications.UserSpecifications.isCreatedBefore;

import java.util.Collection;
import java.util.Date;

import org.joda.time.DateTime;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import tt.showcase.spring.test.common.AbstractSpringTest;
import tt.showcase.spring.user.User;
import tt.showcase.spring.user.springjpa.specifications.UserJpaSpecificationsRepository;

public class UserJpaSpecificationsRepositoryTest extends AbstractSpringTest {

	@Autowired
	private UserJpaSpecificationsRepository userRepository;

	@Test
	public void checkUsersCreatedBeforeDate() {
		Collection<User> users = userRepository
				.findAll(isCreatedBefore(oneYearAgo()));

		for (User user : users) {
			assertThat(user.getCreationDate().compareTo(oneYearAgo()),
					is(lessThan(0)));
		}
	}

	private Date oneYearAgo() {
		return new DateTime().withTimeAtStartOfDay().minusYears(1).toDate();
	}

	@Test
	public void checkUsersWithGivenFirstnameOrAtGivenAge() {
		Collection<User> users = userRepository.findAll(where(atAge(30)).or(
				hasFirstName("Obi")));

		for (User user : users) {
			assertThat(
					user,
					either(hasProperty("firstname", is("Obi"))).or(
							hasProperty("age", is(30))));
		}
	}

	@Test
	public void checkUsersInTheirTwenties() {
		Collection<User> users = userRepository.findAll(atAgeBetween(20, 30));
		for (User user : users) {
			assertThat(user.getAge(), is(both(greaterThan(19))
					.and(lessThan(31))));
		}
	}
}
