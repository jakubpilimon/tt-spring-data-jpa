package tt.showcase.spring.test.address.repositories.pagination;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import tt.showcase.spring.address.Address;
import tt.showcase.spring.address.springjpa.AddressPagingAndSortingRepository;
import tt.showcase.spring.test.common.AbstractSpringTest;

public class AddressPaginationRepositoryTest extends AbstractSpringTest {

	@Autowired
	private AddressPagingAndSortingRepository addressRepository;

	@Test
	public void checkThatPageSizeIsCorrect() {
		Page<Address> firstPageOfSizeFour = addressRepository
				.findAll(new PageRequest(1, 4));
		assertThat(firstPageOfSizeFour.getContent(), hasSize(4));

	}
	@Test
	public void checkThatPageHasOnlyAddressesInOneCity() {
		List<Address> addressesInWwa = addressRepository.findByCity("Wwa",
				new PageRequest(1, 4));
		assertThat(addressesInWwa, hasSize(4));

		for (Address adr : addressesInWwa) {
			assertThat(adr.getCity(), is("Wwa"));
		}
	}

	@Test
	public void checkThatResultIsCorrectlySorted() {
		Iterable<Address> sortedAddresses = addressRepository.findAll(new Sort(
				Sort.Direction.ASC, "street"));

		assertThat(sortedAddresses.iterator().next().getStreet(),
				is("Adama i Ewy"));
	}
}
