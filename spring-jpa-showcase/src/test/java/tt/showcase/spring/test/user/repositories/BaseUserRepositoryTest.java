package tt.showcase.spring.test.user.repositories;

import static org.hamcrest.Matchers.both;
import static org.hamcrest.Matchers.either;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.isIn;
import static org.hamcrest.Matchers.lessThan;
import static org.hamcrest.Matchers.startsWith;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;

import java.util.Collection;
import java.util.Collections;
import java.util.Date;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;

import tt.showcase.spring.test.common.AbstractSpringTest;
import tt.showcase.spring.user.User;
import tt.showcase.spring.user.UserRepository;

public abstract class BaseUserRepositoryTest extends AbstractSpringTest {

	private UserRepository userRepository;

	public abstract UserRepository getUserRepository();

	@Before
	public void setup() {
		userRepository = getUserRepository();
	}

	@Test
	public void checkThatlocalhostExists() {
		assertNotNull(userRepository.findByIpAddress("127.0.0.1"));
	}

	@Test
	public void checkThatObiWanKenobiExists() {
		User user = userRepository.findByFirstnameAndLastname("Obi",
				"Wan Kenobi");
		assertThat(user.getFirstname(), is("Obi"));
		assertThat(user.getLastname(), is("Wan Kenobi"));
	}

	@Test
	public void checkThatObiAndVaderExists() {
		Collection<User> users = userRepository.findByFirstnameOrLastname(
				"Obi", "Vader");

		for (User user : users) {
			assertThat(
					user,
					either(hasProperty("firstname", is("Obi"))).or(
							hasProperty("lastname", is("Vader"))));
		}
	}

	@Test
	public void checkUsersInTheirTwenties() {
		Collection<User> users = userRepository.findByAgeBetween(20, 30);
		for (User user : users) {
			assertThat(user.getAge(), is(both(greaterThan(19))
					.and(lessThan(31))));
		}
	}

	@Test
	public void checkUsersCreatedLessThanYearAgo() {
		Date oneYearAgo = getOneYearAgo();
		Collection<User> users = userRepository
				.findByCreationDateAfter(oneYearAgo);
		for (User user : users) {
			assertThat(user.getCreationDate().compareTo(oneYearAgo),
					is(greaterThan(0)));
		}
	}

	protected Date getOneYearAgo() {
		return new DateTime().withTimeAtStartOfDay().minusYears(1).toDate();
	}

	@Test
	public void checkNotModifiedUsers() {
		Collection<User> users = userRepository.findByModificationDateIsNull();
		for (User user : users) {
			assertNull(user.getModificationDate());
		}
	}

	@Test
	public void checkUsersWithFirstNameLike() {
		Collection<User> users = userRepository
				.findByFirstnameStartingWith("Dart");
		for (User user : users) {
			assertThat(user.getFirstname(), startsWith("Dart"));
		}
	}

	@Test
	public void checkUsersInCertainAge() {
		Collection<Integer> ages = Collections.singleton(new Integer(111));
		Collection<User> users = userRepository.findByAgeIn(ages);
		for (User user : users) {
			assertThat(user.getAge(), isIn(ages));
		}
	}

	@Test
	public void checkActiveUsers() {
		Collection<User> users = userRepository.findByActiveTrue();
		for (User user : users) {
			assertThat(user.getActive(), is(true));
		}
	}

}
