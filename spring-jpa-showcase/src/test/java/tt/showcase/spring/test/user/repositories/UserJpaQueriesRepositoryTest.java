package tt.showcase.spring.test.user.repositories;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import tt.showcase.spring.user.UserRepository;

public class UserJpaQueriesRepositoryTest extends BaseUserRepositoryTest {

	@Autowired
	@Qualifier("jpaQueries")
	private UserRepository userRepository;

	@Override
	public UserRepository getUserRepository() {
		return userRepository; 
	}

}
